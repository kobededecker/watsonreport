# watsonreport

This tool can be used to generate an excel report with hours logged on each project summarized per day.

## installing
run `pip install watsonreport`

This command will also install the [td-watson](https://tailordev.github.io/Watson/) python library.

## use
Use watson like explained on watson's website.

Generate an excel report for summary per project/day by running since a certain date.

`watsonreport 2018-10-21`


